----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date: 04/07/2017 03:43:07 AM
-- Design Name: 
-- Module Name: IFU - Behavioral
-- Project Name: 
-- Target Devices: 
-- Tool Versions: 
-- Description: 
-- 
-- Dependencies: 
-- 
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
-- 
----------------------------------------------------------------------------------


library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.STD_LOGIC_ARITH.ALL;
use IEEE.STD_LOGIC_UNSIGNED.ALL;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx leaf cells in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity IFU is
    Port ( clk : in STD_LOGIC;
           rst : in STD_LOGIC;
           we  : in STD_LOGIC;
           jmp : in STD_LOGIC;
         pcsrc : in STD_LOGIC;
        braddr : in STD_LOGIC_VECTOR (15 downto 0);
       jmpaddr : in STD_LOGIC_VECTOR (15 downto 0);
         instr : out STD_LOGIC_VECTOR (15 downto 0);
            pc : out STD_LOGIC_VECTOR (15 downto 0) );
end IFU;

architecture Behavioral of IFU is

type ROM is array ( 0 to 255 ) of STD_LOGIC_VECTOR(15 downto 0);
signal rom1 : ROM := ( B"000_001_001_001_0_111", -- XOR $1, $1, $1 0497
                       B"001_001_100_0001000", -- ADDI $1, $4, 8 2608
                       B"000_010_010_010_0_111", -- XOR $2, $2, $2 0927
                       B"000_100_100_100_1_011", -- SRL $4, $4, 1 1246
                       B"001_010_010_0000001", -- ADDI $2, $2, 1 2901
                       B"011_010_010_0000000", -- SW $2, offset $2 6900
                       B"010_001_001_0000000",-- LW $1, offset $1 4480
                       B"000_010_010_010_0_111", --XOR $2, $2, $2 0927
                       B"000_100_100_100_0_111", --XOR $4, $4, $4 1247
                       B"110_100_010_0000011", -- BEQ $4, $2, 3 9103
                       B"111_0000000000011", -- J 3 E003
                       others => "0000000000000000");
                      
                      
--add:	B"000_001_000_010_0_001"  X"0420"   R[1] = R[0] + R[2]
--sub:  B"000_001_010_010_0_010"  X"2521"   R[1] = R[2] - R[2]
--sll:  B"000_010_000_010_1_011"  X"482A"   R[2] = R[2] << 1
--srl:  B"000_010_000_010_1_100"  X"682B"   R[2] = R[2] >> 1
--and:  B"000_011_010_100_0_101"  X"4D44"   
--or:   B"000_101_100_100_0_110"  X"F645" 
--xor:  B"000_100_100_100_0_111"  X"9246" 

--addi: B"001_000_100_0000100"  X"2204"  
--lw:   B"010_001_101_0000000"  X"4680"  
--sw:   B"011_100_101_0000000"  X"7280"  
--beq:  B"100_001_001_0000001"  X"8481"  
--andi: B"101_100_101_0000100"  X"B284"  
--ori:  B"110_101_110_0000011"  X"D703" 
--j     B"111_0000000000011"    X"E003"                        
                      
           
signal s1, s2, s3, s4 : STD_LOGIC_VECTOR ( 15 downto 0 );

begin

process( jmp, s1, jmpaddr, s2 )
begin
    case (jmp) is
        when '1' => s2 <= jmpaddr;
        when '0' => s2 <= s1;
        when others => s2 <= X"0000";
    end case;
end process;

process(pcsrc, s3, braddr, s1)
begin
    case (pcsrc) is
        when '0' => s1 <= s3;
        when '1' => s1 <= braddr;
        when others => s2 <= X"0000";
     end case;
end process;

process(clk, we, rst)
begin
    if rst = '1' then
        s4 <= X"0000";
    else if we = '1' and rising_edge(clk) then
        s4 <= s2;
         end if;
    end if;
end process;

s3 <= s4 + '1';
pc <= s3;
instr <= rom1(conv_integer(s4(7 downto 0)));

end Behavioral;
