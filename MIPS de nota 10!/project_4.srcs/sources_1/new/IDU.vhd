----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date: 04/07/2017 09:31:52 AM
-- Design Name: 
-- Module Name: DEC - Behavioral
-- Project Name: 
-- Target Devices: 
-- Tool Versions: 
-- Description: 
-- 
-- Dependencies: 
-- 
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
-- 
----------------------------------------------------------------------------------


library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.STD_LOGIC_ARITH.ALL;
use IEEE.STD_LOGIC_UNSIGNED.ALL;
use IEEE.NUMERIC_STD.all;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx leaf cells in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity IDU is
    Port ( clk : in STD_LOGIC;
           instr : in STD_LOGIC_VECTOR (15 downto 0);
           regdst : in STD_LOGIC;
           regwr : in STD_LOGIC;
           extop : in STD_LOGIC;
           wd : in STD_LOGIC_VECTOR (15 downto 0);
           rd1 : out STD_LOGIC_VECTOR (15 downto 0);
           rd2 : out STD_LOGIC_VECTOR (15 downto 0);
           extimm : out STD_LOGIC_VECTOR (15 downto 0);
           func : out STD_LOGIC_VECTOR (2 downto 0);
           sa : out STD_LOGIC);
end IDU;

architecture Behavioral of IDU is

component REG_FILE is
    Port ( clk : in std_logic;
           ra1 : in std_logic_vector (3 downto 0);
           ra2 : in std_logic_vector (3 downto 0);
            wa : in std_logic_vector (3 downto 0);
            wd : in std_logic_vector (15 downto 0);
           wen : in std_logic;
           rd1 : out std_logic_vector (15 downto 0);
           rd2 : out std_logic_vector (15 downto 0));
end component;

--signal s1 : STD_LOGIC_VECTOR (6 downto 0);
--signal sa : STD_LOGIC_VECTOR (2 downto 0);
signal ra1, ra2, s2 : STD_LOGIC_VECTOR (3 downto 0);

begin

process(regdst, instr(9 downto 4))
begin
    case (regdst) is
        when '1' => s2 <= "0" & instr(12 downto 10);
        when '0' => s2 <= "0" & instr(9 downto 7);
        when others => s2 <= X"0000";
    end case;
end process;

ra1 <= "0" & instr(9 downto 7);
ra2 <= "0" & instr(6 downto 4);

C1 : REG_FILE port map (clk => clk, ra1 => ra1, ra2 => ra2, wa => s2, wd => wd, wen => regwr, rd1 => rd1 , rd2 => rd2);

process(extop, instr(7 downto 0))
begin
    if extop='0' then
        extimm <= "00000000" & instr(7 downto 0);
    else
        if instr(6)='1' then
            extimm <= "11111111" & instr(7 downto 0);
        else 
            extimm <= "00000000" & instr(7 downto 0);
        end if;
    end if;
end process;

sa <= instr(3);
func <= instr(2 downto 0);


end Behavioral;
