----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date: 04/22/2016 04:28:28 PM
-- Design Name: 
-- Module Name: mainControl - Behavioral
-- Project Name: 
-- Target Devices: 
-- Tool Versions: 
-- Description: 
-- 
-- Dependencies: 
-- 
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
-- 
----------------------------------------------------------------------------------

library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.STD_LOGIC_ARITH.ALL;
use IEEE.STD_LOGIC_UNSIGNED.ALL;
-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx leaf cells in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity MCU is
    Port ( instr : in STD_LOGIC_VECTOR (2 downto 0);
           regdst : out STD_LOGIC;
           extop : out STD_LOGIC;
           alusrc : out STD_LOGIC;
           branch : out STD_LOGIC; -- PSrc
           jmp : out STD_LOGIC;  
           aluop : out STD_LOGIC_VECTOR(2 downto 0);
           memwr: out STD_LOGIC;
           memtoreg: out STD_LOGIC;
           regwr: out STD_LOGIC);  
end MCU;

architecture Behavioral of MCU is

begin

process(instr)
begin
    case instr is
        when "000" => -- R-Type
                    regdst <= '1';  
                    extop <= '0';   
                    alusrc <= '0';  
                    branch <= '0';  
                    jmp <= '0';     
                    aluop <= "000"; 
                    memwr <= '0';   
                    regwr <= '1';   
                    memtoreg <= '0';
                    
        when "001" => -- addi
                    regdst <= '0'; 
                    extop <= '1';   
                    alusrc <= '1';  
                    branch <= '0';  
                    jmp <= '0';     
                    aluop <= "001"; 
                    memwr <= '0';   
                    regwr <= '1';   
                    memtoreg <= '0';
   
        when "010" => --lw
                    regdst <= '0';  
                    extop <= '1';   
                    alusrc <= '1';  
                    branch <= '0';  
                    jmp <= '0';     
                    aluop <= "010"; 
                    memwr <= '0';   
                    regwr <= '1';   
                    memtoreg <= '1';
                                      
        when "011" => -- sw
                    regdst <= '0';  
                    extop <= '1';   
                    alusrc <= '1';  
                    branch <= '0';  
                    jmp <= '0';     
                    aluop <= "011"; 
                    memwr <= '1';   
                    regwr <= '0';   
                    memtoreg <= '0';
         	
       when "100"=> -- andi                  
                    regdst<='0';                  
                    extop<='1';                   
                    alusrc<='1';                  
                    branch<='0';                  
                    jmp<='0';                    
                    aluop<="100";                 
                    memwr<='0';                
                    memtoreg<='0';                
                    regwr<='1';                
                                                    
       when "101"=> -- ori       
                    regdst<='0';                 
                    extop<='1';                  
                    alusrc<='1';                 
                    branch<='0';                 
                    jmp<='0';                   
                    aluop<="101";                
                    memwr<='0';               
                    memtoreg<='0';               
                    regwr<='1';                      
                      
        when "110" => -- beq
                    regdst <= '0';  
                    extop <= '1';   
                    alusrc <= '0';  
                    branch <= '1';  
                    jmp <= '0';     
                    aluop <= "110"; 
                    memwr <= '0';   
                    regwr <= '0';   
                    memtoreg <= '0'; 
     
        when "111" => -- jump
                    regdst <='0';  
                    extop <='1';  
                    alusrc<='0';  
                    branch<='0';  
                    jmp<='1';     
                    aluop<="111"; 
                    memwr<='0';   
                    regwr<='0';   
                    memtoreg<='0';
        
        when others => -- null
                    regdst<='X';
                    extop<='X';
                    alusrc<='X';  
                    branch<='0';  
                    jmp<='0';     
                    aluop<="000"; 
                    memwr<='0';   
                    memtoreg<='0';
                    regwr<='0';              
   end case;                 
end process;           

end Behavioral;
