----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date: 03/17/2017 08:55:05 AM
-- Design Name: 
-- Module Name: test_env - Behavioral
-- Project Name: 
-- Target Devices: 
-- Tool Versions: 
-- Description: 
-- 
-- Dependencies: 
-- 
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
-- 
----------------------------------------------------------------------------------


library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.STD_LOGIC_ARITH.ALL;
use IEEE.STD_LOGIC_UNSIGNED.ALL;
use IEEE.NUMERIC_STD.all;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx leaf cells in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity test_env is
    Port ( btn : in STD_LOGIC_VECTOR (4 downto 0);
           sw  : in STD_LOGIC_VECTOR (15 downto 0);
           clk : in STD_LOGIC;
           an : out STD_LOGIC_VECTOR (3 downto 0);
           cat : out STD_LOGIC_VECTOR (6 downto 0);
          led : out STD_LOGIC_VECTOR (15 downto 0));
end test_env;

architecture Behavioral of test_env is

component MPG is
    Port ( btn : in STD_LOGIC;
           clk : in STD_LOGIC;
           en  : out STD_LOGIC);
end component;

component SSD is
    Port ( clk : in STD_LOGIC;
           sw  : in STD_LOGIC_VECTOR (15 downto 0);
           cat : out STD_LOGIC_VECTOR (6 downto 0);
           an  : out STD_LOGIC_VECTOR (3 downto 0));
end component;

component RAM is
    Port ( clk : in STD_LOGIC;
           en  : in STD_LOGIC;
           we  : in STD_LOGIC;
           addr : in STD_LOGIC_VECTOR ( 15 downto 0 );
           wdata: in STD_LOGIC_VECTOR ( 15 downto 0 );
           rdata: out STD_LOGIC_VECTOR ( 15 downto 0 ));
end component;

component IDU is
    Port ( clk : in STD_LOGIC;
           instr : in STD_LOGIC_VECTOR (15 downto 0);
           regdst : in STD_LOGIC;
           regwr : in STD_LOGIC;
           extop : in STD_LOGIC;
           wd : in STD_LOGIC_VECTOR (15 downto 0);
           rd1 : out STD_LOGIC_VECTOR (15 downto 0);
           rd2 : out STD_LOGIC_VECTOR (15 downto 0);
           extimm : out STD_LOGIC_VECTOR (15 downto 0);
           func : out STD_LOGIC_VECTOR (2 downto 0);
           sa : out STD_LOGIC);
end component;

component IFU is
    Port ( clk : in STD_LOGIC;
           rst : in STD_LOGIC;
            we : in STD_LOGIC;
           jmp : in STD_LOGIC;
         pcsrc : in STD_LOGIC;
        braddr : in STD_LOGIC_VECTOR ( 15 downto 0 );
       jmpaddr : in STD_LOGIC_VECTOR ( 15 downto 0 );
         instr : out STD_LOGIC_VECTOR ( 15 downto 0 );
            pc : out STD_LOGIC_VECTOR ( 15 downto 0 ) );
end component;

component MCU is
    Port ( instr : in STD_LOGIC_VECTOR (2 downto 0);
          regdst : out STD_LOGIC;
           extop : out STD_LOGIC;
          alusrc : out STD_LOGIC;
          branch : out STD_LOGIC; -- PSrc
             jmp : out STD_LOGIC;  
           aluop : out STD_LOGIC_VECTOR (2 downto 0);
           memwr : out STD_LOGIC;
        memtoreg : out STD_LOGIC;
           regwr : out STD_LOGIC );  
end component;

component EU is
    Port ( pc : in std_logic_vector ( 15 downto 0 );
          rd1 : in std_logic_vector ( 15 downto 0 );
          rd2 : in std_logic_vector ( 15 downto 0 );
       extimm : in std_logic_vector ( 15 downto 0 );
         func : in std_logic_vector ( 2 downto 0 );
           sa : in std_logic;
       alusrc : in std_logic;
        aluop : in std_logic_vector ( 2 downto 0 );
   branchaddr : out std_logic_vector ( 15 downto 0 );
       alures : out std_logic_vector ( 15 downto 0 );
         zero : out std_logic);
end component;

signal wen, rst, regdst, alusrc, pcsrc, sa, zero, branch, jmp, memwr, regwr1, regwr0, memtoreg, extop, regwr : STD_LOGIC;
signal aluop : STD_LOGIC_VECTOR ( 2 downto 0 );
signal s1, instr, pc, wd, rd1, memdata, rd2, extimm, alures, branchaddr, jmpaddr : STD_LOGIC_VECTOR (15 downto 0);
signal func : STD_LOGIC_VECTOR ( 2 downto 0 );

begin

C1: MPG port map ( btn(0), clk, rst );
C2: MPG port map ( btn(1), clk, wen );
C3: IFU port map ( clk, rst, wen, jmp, pcsrc, branchaddr, jmpaddr, instr, pc );
C4: IDU port map ( clk, instr, regdst, regwr1, extop, wd, rd1, rd2, extimm, func, sa );
C5: MCU port map ( instr ( 15 downto 13 ), regdst, extop, alusrc, branch, jmp, aluop, memwr, memtoreg, regwr );
C6: SSD port map ( clk, s1, cat, an );
C7: EU  port map ( pc, rd1, rd2, extimm, func, sa, alusrc, aluop, branchaddr, alures, zero );
C8: RAM port map ( clk, sw(2), memwr, alures, rd2, memdata );

process(memtoreg, memdata, alures)
begin
    case memtoreg is
        when '1' => wd <= memdata;
        when '0' => wd <= alures;
        when others => wd <= alures;
    end case;
end process;

--process(wen, regwr1, regwr, regwr0)
--begin
--    case (wen) is
--        when '0' => regwr1 <= regwr;
--        when '1' => regwr1 <= regwr0;
--        when others => regwr1 <= '0';
--    end case;
--end process;

jmpaddr <= pc ( 15 downto 13 ) & instr ( 12 downto 0 );
regwr1 <= regwr and wen;
pcsrc <= branch and zero;

--wd <= rd1 + rd2;

process( sw ( 7 downto 5 ), instr, pc, rd1, rd2, wd )
begin
    case sw ( 7 downto 5 ) is
        when "000" => s1 <= instr;
        when "001" => s1 <= pc;   
        when "010" => s1 <= rd1;  
        when "011" => s1 <= rd2;  
        when "100" => s1 <= extimm;
        when "101" => s1 <= alures;   
        when "110" => s1 <= memdata;
        when "111" => s1 <= wd;
    when others => s1 <= X"0000";
    end case;
end process;

led(15 downto 13)<=aluop;
led(12 downto 10)<=func;
led(9)<=pcsrc;
led(8)<=extop;
led(7)<=regdst;
led(6)<=jmp;
led(5)<=branch;
led(3)<=memtoreg;
led(2)<=memwr;
led(1)<=alusrc;
led(0)<=regwr;

end Behavioral;
