----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date: 03/24/2017 03:40:16 AM
-- Design Name: 
-- Module Name: RAM - Behavioral
-- Project Name: 
-- Target Devices: 
-- Tool Versions: 
-- Description: 
-- 
-- Dependencies: 
-- 
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
-- 
----------------------------------------------------------------------------------


library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.STD_LOGIC_ARITH.ALL;
use IEEE.STD_LOGIC_UNSIGNED.ALL;
use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx leaf cells in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity RAM is
    Port ( clk : in STD_LOGIC;
           en  : in STD_LOGIC;
           we  : in STD_LOGIC;
           addr : in STD_LOGIC_VECTOR ( 15 downto 0 );
           wdata: in STD_LOGIC_VECTOR ( 15 downto 0 );
           rdata: out STD_LOGIC_VECTOR ( 15 downto 0 ));
end RAM;

architecture Behavioral of RAM is

type RAM is array ( 0 to 15 ) of STD_LOGIC_VECTOR(15 downto 0);
signal ram1 : RAM :=( x"0000",
                      x"0001", x"0002",x"0003",x"0004",
                      x"0005", x"0006",x"0007",x"0008",
                      x"0009", x"0010",x"0011",x"0012",
                      x"0013", x"0014",
                      others => "0000000000000000");

begin

process(clk, en, we, addr, ram1)
begin
    if (en = '1') then 
        if rising_edge(clk) then
            if (we = '1') then
                ram1(CONV_INTEGER(addr)) <= wdata;
                rdata <= wdata;
            end if;
        end if;    
        rdata <= ram1(CONV_INTEGER(addr)); 
    end if;
end process;

end Behavioral;
