----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date: 03/17/2017 08:55:05 AM
-- Design Name: 
-- Module Name: test_env - Behavioral
-- Project Name: 
-- Target Devices: 
-- Tool Versions: 
-- Description: 
-- 
-- Dependencies: 
-- 
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
-- 
----------------------------------------------------------------------------------

library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.STD_LOGIC_ARITH.ALL;
use IEEE.STD_LOGIC_UNSIGNED.ALL;
use IEEE.NUMERIC_STD.all;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx leaf cells in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity REG_FILE is
    Port ( clk : in std_logic;
           ra1 : in std_logic_vector (3 downto 0);
           ra2 : in std_logic_vector (3 downto 0);
            wa : in std_logic_vector (3 downto 0);
            wd : in std_logic_vector (15 downto 0);
           wen : in std_logic;
           rd1 : out std_logic_vector (15 downto 0);
           rd2 : out std_logic_vector (15 downto 0));
end REG_FILE;

architecture Behavioral of reg_file is
type reg_array is array (0 to 15) of std_logic_vector(15 downto 0);
signal reg : reg_array := (x"0000",
                      x"0007", x"0002", x"0003", x"0004",
                      x"0005", x"0006", x"0007", x"0008",
                      x"0009", x"0010", x"0011", x"0012",
                      x"0013", x"0014",
                      others => "0000000000000000");
begin

    process(clk)
    begin
        if rising_edge(clk) then
            if wen = '1' then
                reg(conv_integer(wa)) <= wd;
            end if;
        end if;
    end process;
    rd1 <= reg(conv_integer(ra1));
    rd2 <= reg(conv_integer(ra2));
    
end Behavioral; 