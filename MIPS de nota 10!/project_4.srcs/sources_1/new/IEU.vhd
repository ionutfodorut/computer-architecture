----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date:    08:10:01 04/14/2017 
-- Design Name: 
-- Module Name:    IEU 
-- Project Name: 
-- Target Devices: 
-- Tool versions: 
-- Description: 
--
-- Dependencies: 
--
-- Revision: 
-- Revision 0.01 - File Created
-- Additional Comments: 
--
----------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.STD_LOGIC_ARITH.ALL;
use IEEE.STD_LOGIC_UNSIGNED.ALL;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx primitives in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity EU is
Port(
	pc:in std_logic_vector(15 downto 0);
    rd1: in std_logic_vector(15 downto 0);
    rd2: in std_logic_vector(15 downto 0);
    extimm: in std_logic_vector(15 downto 0);
    func: in std_logic_vector(2 downto 0);
    sa: in std_logic;
    alusrc: in std_logic;
    aluop: in std_logic_vector(2 downto 0);
    branchaddr: out std_logic_vector(15 downto 0);
    alures: out std_logic_vector(15 downto 0);
    zero: out std_logic);
end EU;

architecture Behavioral of EU is

signal aluin, alures1 : std_logic_vector(15 downto 0);
signal aluctrl: std_logic_vector(3 downto 0);
signal s1: std_logic_vector(15 downto 0);
signal s2, zero1: std_logic;

begin

branchaddr <= pc + extimm;

process(alusrc, aluin, rd2, extimm)
begin
    case alusrc is
        when '0' => aluin <= rd2;
        when '1' => aluin <= extimm;
        when others => aluin <= extimm;
    end case;
end process;
			  

process(aluop, func)
begin
	case (aluop) is
		when "000"=>
				case (func) is
					when "001" => aluctrl <= "0000"; 	--add
					when "010" => aluctrl <= "0001";   --sub
					when "011" => aluctrl <= "0010";   --sll
					when "100" => aluctrl <= "0011";   --srl
					when "101" => aluctrl <= "0100";	--and
					when "110" => aluctrl <= "0101";	--or
					when "111" => aluctrl <= "0110";   --xor
					when others => aluctrl <= "0000";	
				end case;
		when "001" => aluctrl <= "0000";		--addi
		when "010" => aluctrl <= "1010";		--lw
		when "011" => aluctrl <= "1010";        --sw
		when "100" => aluctrl <= "0100";		--andi
		when "101" => aluctrl <= "0101";		--ori
		when "110" => aluctrl <= "0001";        --beq
		when "111" => aluctrl <= "1000";		--jmp
		when others => aluctrl <= "0000";	
	end case;
end process;

process(aluctrl, rd1, aluin, sa)
begin
	case (aluctrl) is
		when "0000" => alures1 <= rd1 + aluin;   --add
		when "0001" => alures1 <= rd1 - aluin;   --sub
		when "0010" => 				       --sll
					case (sa) is
						when '1' => alures1 <= rd1 ( 14 downto 0 ) & "0";
						when others => alures1 <= rd1;	
					end case;	
		when "0011" => 				       --srl
					case (sa) is
						when '1' => alures1 <= "0" & rd1 ( 15 downto 1 );
						when others => alures1 <= rd1;
					end case;	
		when "0100" => alures1 <= rd1 and aluin; --and
		when "0101" => alures1 <= rd1 or aluin;  --or
		when "0110" => alures1 <= rd1 xor aluin; --xor
		when "1000" => alures1 <= X"0000";	    --jmp
		when "1010" => alures1 <= rd1 + aluin;   --lw, sw
		when others => alures1 <= X"0000";	 
	end case;

	case (alures1) is
		when X"0000" => zero <= '1';
		when others => zero <= '0';
	end case;
end process;

alures <= alures1;
	
--zero<=zero1;			-----ZERO_OUT-----

--alures<=alures1;		-----ALU_OUT-----

end Behavioral;
