----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date: 03/10/2017 01:42:42 AM
-- Design Name: 
-- Module Name: MPG - Behavioral
-- Project Name: 
-- Target Devices: 
-- Tool Versions: 
-- Description: 
-- 
-- Dependencies: 
-- 
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
-- 
----------------------------------------------------------------------------------


library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.STD_LOGIC_ARITH.ALL;
use IEEE.STD_LOGIC_UNSIGNED.ALL;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx leaf cells in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity MPG is
    Port ( btn : in STD_LOGIC;
           clk : in STD_LOGIC;
            en : out STD_LOGIC );
end MPG;

architecture Behavioral of MPG is

signal count : STD_LOGIC_VECTOR (15 downto 0);
signal s0, s1, s2, s3 : STD_LOGIC;

begin

process(clk)
begin
    if clk = '1' and clk'event then
          count <= count + 1;
    end if;
end process;

process(count)
begin
    if count = "1111111111111111" then
        s0 <= '1';
    else 
        s0 <= '0';
    end if;
end process;

process(clk)
begin
    if clk = '1' and clk'event then
        if s0 = '1' then
            s1 <= btn; 
        end if;
    end if;
end process;

process(clk)
begin
    if clk = '1' and clk'event then
        s2 <= s1; 
    end if;
end process;

process(clk)
begin
    if clk = '1' and clk'event then
        s3 <= s2; 
    end if;
end process;

en <= s2 and (not s3);
end Behavioral;
